#!/bin/bash
# Upendra Kumar Devisetty
# 02/24/16
# Script to process cuffcompare output file to generate lincRNA
# Usage: 
# sh evolinc-part-I.sh -c sample.data/cuffcompare_out_annot_no_annot.combined.gtf -g sample.data/Brassica_rapa_v1.2_genome.fa -r sample.data/Brassica_rapa_v1.2_cds.fa -b sample.data/TE_RNA_transcripts.fa 2> errors.txt > output.txt

usage() {
      echo ""
      echo "Usage : sh $0 -c cuffcompare -g genome -r gff -o output [-b TE_RNA] [-t CAGE_RNA] [-x Known_lincRNA]"
      echo ""

cat <<'EOF'
  -c </path/to/cuffcompare output file>

  -g </path/to/reference genome file>

  -r </path/to/reference annotation file>

  -b </path/to/Transposable Elements file>

  -o </path/to/output file>

  -t </path/to/CAGE RNA file>
  
  -x </path/to/Known lincRNA file>

  -h Show this usage information

EOF
    exit 0
}

while getopts ":b:c:g:hr:t:x:o:" opt; do
  case $opt in
    b)
      blastfile=$OPTARG
      ;;
    c)
      comparefile=$OPTARG
      ;;
    h)
      usage
      exit 1
      ;;    
    g)
     referencegenome=$OPTARG
      ;;
    r)
     referencegff=$OPTARG
      ;;  
    t)
     cagefile=$OPTARG
      ;;
    x)
     knownlinc=$OPTARG
      ;;
    o)
     output=$OPTARG
      ;;  
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Download all the dependencies
# Cufflinks
wget -O- http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/cufflinks-2.2.1.Linux_x86_64.tar.gz | tar xzvf -

# Transdecoder
wget -O- https://github.com/TransDecoder/TransDecoder/archive/2.0.1.tar.gz | tar xzvf -

# Ncbi BLAST
curl ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.3.0+-x64-linux.tar.gz > ncbi-blast-2.3.0+-x64-linux.tar.gz
tar xvf ncbi-blast-2.3.0+-x64-linux.tar.gz

# Python
wget https://www.python.org/ftp/python/2.7.10/Python-2.7.10.tgz
tar -zxvf Python-2.7.10.tgz
cd Python-2.7.10
./configure
make
cd ..

# BWA
wget --no-check-certificate https://sourceforge.net/projects/bio-bwa/files/latest/download?source=files
tar xvf bwa-0.7.12.tar.bz2
cd bwa-0.7.12
make
cd ..

# Samtools
wget --no-check-certificate http://sourceforge.net/projects/samtools/files/samtools/1.0/samtools-bcftools-htslib-1.0_x64-linux.tar.bz2/download
tar xvf samtools-bcftools-htslib-1.0_x64-linux.tar.bz2

# Bedtools
wget https://github.com/arq5x/bedtools2/archive/v2.25.0.tar.gz
tar xvf v2.25.0
cd bedtools2-2.25.0
make
cd ..

# Quast
wget -O- http://ftp.mirrorservice.org/sites/download.sourceforge.net/pub/sourceforge/q/qu/quast/quast-3.0.tar.gz | tar zxvf -

# cpan library
curl -L http://cpanmin.us | perl - App::cpanminus
cpanm URI/Escape.pm


# Create a directory to move all the output files
mkdir $output

# STEP 1:
# Extracting classcode u transcripts, making fasta file, removing transcripts > 200 and selecting protein coding transcripts
grep -e '"u"' -e '"x"' -e '"s"' -e '"o"' -e '"e"' -e '"i"' $comparefile | cufflinks-2.2.1.Linux_x86_64/gffread -w \
    transcripts_u.fa -g $referencegenome - && Python-2.7.10/python get_gene_length_filter.py transcripts_u.fa \
    transcripts_u_filter.fa && TransDecoder-2.0.1/TransDecoder.LongOrfs -t transcripts_u_filter.fa


# Modifying the header
sed 's/ .*//' transcripts_u_filter.fa | sed -ne 's/>//p' > transcripts_u_filter.fa.genes

# Move the transcript files to this directory transcripts_u_filter.fa.transdecoder_dir
mv transcripts_u.fa transcripts_u_filter.fa transcripts_u_filter.fa.genes transcripts_u_filter.fa.transdecoder_dir/

# Change the directory 
cd transcripts_u_filter.fa.transdecoder_dir

# Genes in the protein coding genes
sed 's/|.*//' longest_orfs.cds | sed -ne 's/>//p' | uniq > longest_orfs.cds.genes

# Remove these protein coding genes from the filter file
grep -v -f longest_orfs.cds.genes transcripts_u_filter.fa.genes > transcripts_u_filter.not.genes && \
  sed 's/^/>/' transcripts_u_filter.not.genes > temp && mv temp transcripts_u_filter.not.genes # changed name here 

# Extract fasta file
../Python-2.7.10/python ../extract_sequences.py transcripts_u_filter.not.genes transcripts_u_filter.fa transcripts_u_filter.not.genes.fa && \
  sed 's/ /./' transcripts_u_filter.not.genes.fa > temp && mv temp transcripts_u_filter.not.genes.fa

# Blast the fasta file to TE RNA db
for i in "$@" ; do
  if [[ $i == "$blastfile" ]] ; 
  then
     ../ncbi-blast-2.3.0+/bin/makeblastdb -in ../$blastfile -dbtype nucl -out ../$blastfile.blast.out &&
     ../ncbi-blast-2.3.0+/bin/blastn -query transcripts_u_filter.not.genes.fa -db ../$blastfile.blast.out -out transcripts_u_filter.not.genes.fa.blast.out -outfmt 6 # no blast hits her
  else
    touch transcripts_u_filter.not.genes.fa.blast.out
  fi
done

# Filter the output to select the best transcript based on e-value and bit-score
../Python-2.7.10/python ../filter_sequences.py transcripts_u_filter.not.genes.fa.blast.out transcripts_u_filter.not.genes.fa.blast.out.filtered

# Modify the header in the fasta file to extract header only
grep ">" transcripts_u_filter.not.genes.fa | sed 's/>//' > transcripts_u_filter.not.genes_only

# Now remove the blast hits from the fasta file
../Python-2.7.10/python ../fasta_remove.py transcripts_u_filter.not.genes.fa.blast.out.filtered transcripts_u_filter.not.genes_only lincRNA.genes

# Modify the fasta header to include ">"
sed 's/^/>/' lincRNA.genes > temp && mv temp lincRNA.genes

# Extract the sequences
../Python-2.7.10/python ../extract_sequences-1.py lincRNA.genes transcripts_u_filter.not.genes.fa lincRNA.genes.fa


# STEP 2:
# Convert lincRNA fasta to bed
# Index the genome first
../bwa-0.7.12/bwa index ../$referencegenome ../$referencegenome

# Mapping the lincRNA to genome using bwa
../bwa-0.7.12/bwa bwasw -t 2 ../$referencegenome lincRNA.genes.fa > lincRNA.genes.sam

# Convert sam to bam using samtools
../samtools-bcftools-htslib-1.0_x64-linux/bin/samtools view -Shu lincRNA.genes.sam > lincRNA.genes.bam

# Convert bam to bed using bed tools
../bedtools2-2.25.0/bin/bamToBed -bed12 -i lincRNA.genes.bam > lincRNA.genes.bed

# Intersect bed to remove overlapping genes
../bedtools2-2.25.0/bin/intersectBed -a lincRNA.genes.bed -b ../$referencegff -v > lincRNA.genes.filtered.bed

cut -f 4 lincRNA.genes.filtered.bed | uniq | sed 's/^/>/' > lincRNA.genes.filtered.uniq.genes

../Python-2.7.10/python ../extract_sequences-1.py lincRNA.genes.filtered.uniq.genes lincRNA.genes.fa lincRNA.genes.filtered.genes.fa


# STEP 3 (promoter extraction)
sed 's/>//' lincRNA.genes.filtered.uniq.genes | cut -d "." -f 1 > lincRNA.genes.filtered.uniq.genes.mod

# Extract the coordinates from the cuffcompare file
grep -f lincRNA.genes.filtered.uniq.genes.mod ../$comparefile > lincRNA.genes.filtered.genes.gtf

# Extracting promoter coordinates from the gtf file for the transcripts
../Python-2.7.10/python ../prepare_promoter_gtf.py lincRNA.genes.filtered.genes.gtf lincRNA.genes.filtered.genes.promoters.gtf

# Extracting fasta from the promoter coordinates
../cufflinks-2.2.1.Linux_x86_64/gffread lincRNA.genes.filtered.genes.promoters.gtf -w lincRNA.genes.filtered.genes.promoters.fa -g ../$referencegenome


# STEP 4 (Demographics)
../quast-3.0/quast.py lincRNA.genes.filtered.genes.fa -o lincRNA.genes.filtered.genes_demographics

sed 's/contig/lincRNA/g' lincRNA.genes.filtered.genes_demographics/report.txt > lincRNA.genes.filtered.genes_demographics.txt

../Python-2.7.10/python ../lincRNA_count.py *fa > lincRNA_final_transcripts_counts.txt 


# STEP 5 (Update the gf file) # not working now but should be good
python ../update_gtf.py lincRNA.genes.filtered.genes.fa ../$comparefile lincRNA.genes.filtered.genes_updated.gtf


# STEP 6 (Move all the files to a directory named output)
../bedtools2-2.25.0/bin/sortBed -i lincRNA.genes.filtered.bed > lincRNA.genes.filtered.sorted.bed
cp lincRNA.genes.filtered.sorted.bed lincRNA.genes.filtered.genes.fa lincRNA.genes.filtered.genes.promoters.fa lincRNA_final_transcripts_counts.txt lincRNA.genes.filtered.genes_demographics.txt lincRNA.genes.filtered.genes_updated.gtf ../$output


# optional - 1 CAGE data
for i in "$@" ; do
  if [[ $i == "$cagefile" ]] ; then
        ../Python-2.7.10/python ../gff2bed.py ../$cagefile AnnotatedPEATPeaks.bed &&
        ../bedtools2-2.25.0/bin/sortBed -i AnnotatedPEATPeaks.bed > AnnotatedPEATPeaks.sorted.bed &&
        ../bedtools2-2.25.0/bin/closestBed -a lincRNA.genes.filtered.sorted.bed -b AnnotatedPEATPeaks.sorted.bed -s -D a > closest_output.txt &&       
        ../Python-2.7.10/python ../closet_bed_compare.py closest_output.txt lincRNA.genes.filtered.genes.fa lincRNA.genes.filtered.genes.CAGE.fa &&
        cp lincRNA.genes.filtered.genes.CAGE.fa ../$output
 fi
done

# optional - 2 Known lincRNA
for i in "$@" ; do
  if [[ $i == "$knownlinc" ]] ; then
     ../Python-2.7.10/python ../gff2bed.py ../$knownlinc Atha_known_lncRNAs.bed &&
     ../bedtools2-2.25.0/bin/sortBed -i Atha_known_lncRNAs.bed > Atha_known_lncRNAs.sorted.bed &&
     ../bedtools2-2.25.0/bin/intersectBed -a lincRNA.genes.filtered.sorted.bed -b Atha_known_lncRNAs.sorted.bed > intersect_output.txt &&
     ../Python-2.7.10/python ../interesect_bed_compare.py intersect_output.txt lincRNA.genes.filtered.genes.fa lincRNA.genes.filtered.genes.knownlinc.fa &&
     cp lincRNA.genes.filtered.genes.knownlinc.fa ../$output
  fi
done

# remove all the other files
# rm -r ../transcripts_u_filter.fa.transdecoder_dir

echo "All necessary files written to output"
echo "Finished Evolinc-part-I!"

