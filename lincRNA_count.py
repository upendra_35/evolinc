#!/usr/local/env python
# Upendra Kumar Devisetty
# 11/12/15
# Scrip to count transcript at each step of pipeline

import sys
import os

os.chdir(os.getcwd())

count_1 = 0
count_2 = 0
count_3 = 0
count_4 = 0

#file_out = open("lincRNA_final_counts.txt", "a")

if "transcripts_u.fa" in sys.argv[1:]:
		file_one = open("transcripts_u.fa")
		for line in file_one:
			line = line.strip()
			if line.startswith(">"):
				count_1 += 1
print 'transcripts_u.fa: initial "u" transcripts original count -', count_1 


if "transcripts_u_filter.not.genes.fa" in sys.argv[1:]:
		file_one = open("transcripts_u_filter.not.genes.fa")
		for line in file_one:
			line = line.strip()
			if line.startswith(">"):
				count_2 += 1
print 'transcripts_u_filter.not.genes.fa: after ORF filter -', count_2 


if "lincRNA.genes.fa" in sys.argv[1:]:
		file_one = open("lincRNA.genes.fa")
		for line in file_one:
			line = line.strip()
			if line.startswith(">"):
				count_3 += 1
print 'lincRNA.genes.fa: after TEDB filter -', count_3 


if "lincRNA.genes.filtered.genes.fa" in sys.argv[1:]:
	file_one = open("lincRNA.genes.filtered.genes.fa")
	for line in file_one:
		line = line.strip()
		if line.startswith(">"):
			count_4 += 1

print 'lincRNA_final_transcripts.fa: final LncRNA count -', count_4 

